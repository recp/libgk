/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#include "gk_model.h"
#include "gk_common.h"

void
GK_CONSTRUCTOR
gk__init() {

}

void
GK_DESTRUCTOR
gk__cleanup() {

}
