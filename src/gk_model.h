/*
 * Copyright (c), Recep Aslantas.
 *
 * MIT License (MIT), http://opensource.org/licenses/MIT
 * Full license can be found in the LICENSE file
 */

#ifndef gk_src_model_h
#define gk_src_model_h

#include "../include/gk.h"

void
gk_mdl_init();

void
gk_mdl_deinit();

#endif /* gk_src_model_h */
